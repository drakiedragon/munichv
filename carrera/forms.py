from django import forms
from .models import RegistroCarrera

class FormRegistroCarrera(forms.Form):
    nombre_carrera = forms.CharField(max_length=100)
    min_estudiantes = forms.IntegerField()
    facultad = forms.CharField(max_length=100)
    descripcion_carrera = forms.CharField(max_length=200)

    class Meta:
        model = RegistroCarrera
        fields=["nombre_carrera","min_estudiantes","facultad","descripcion_carrera"]

    def cleaned_nombre_carrera(self):
        nombre = self.cleaned_data.get("nombre_producto")
        if nombre:
            if len(nombre)<=2:
                raise forms.ValidationError("El nombre de la carrera debe contener más de dos caracteres.")
            return nombre
        else:
            raise forms.ValidationError("este campo es requerido")