from django.shortcuts import render
from .forms import FormRegistroCarrera
from .models import RegistroCarrera

def inicio2(request):
    form = FormRegistroCarrera(request.POST or None)

    if form.is_valid():
        form_data= form.cleaned_data
        nombre_carrera2 = form_data.get("nombre_carrera")
        facultad2 = form_data.get("facultad")
        min_estudiantes2 = form_data.get("min_estudiantes")
        descripcion_carrera2 = form.data.get("descripcion_carrera")

        objeto = RegistroCarrera.objects.create(nombre_carrera=nombre_carrera2,facultad=facultad2,min_estudiantes=min_estudiantes2,descripcion_carrera=descripcion_carrera2)

    contexto = {
        "el_formulario": form,
    }
    return render(request, "inicio2.html", contexto)
