from django.contrib import admin
from carrera.models import *
from .forms import FormRegistroCarrera

# Register your models here.
class AdminRegistroCarrera(admin.ModelAdmin):
    list_display = ['nombre_carrera','min_estudiantes','facultad','fecha_apertura','descripcion_carrera']
    search_fields = ['nombre_carrera','facultad','fecha_apertura']
    list_filter = ['facultad']
    #class Meta:
        #model = RegistroCarrera
admin.site.register(RegistroCarrera,AdminRegistroCarrera)
