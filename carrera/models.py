from django.db import models

class RegistroCarrera(models.Model):

    id_carrera = models.AutoField(primary_key=True,unique=True)
    nombre_carrera = models.CharField(max_length=100, blank=False)
    descripcion_carrera = models.CharField(max_length=200, blank=False)
    min_estudiantes = models.IntegerField(blank = False)
    facultad = models.CharField(max_length=100, blank=False)
    fecha_apertura = models.DateTimeField(auto_now_add=True, auto_now=False)

    def __str__(self):
        return self.nombre_carrera