from django.db import models

class RegistrarEstudiante(models.Model):
    id_registro = models.AutoField(primary_key=True, unique=True)
    nombre_estudiante = models.CharField(max_length=100, blank=False)
    edad_estudiante = models.IntegerField(blank=False)
    rut_estudiante = models.IntegerField(blank=False)
    carrera = models.CharField(max_length=100,blank=False)
    fecha_ingreso = models.DateTimeField(auto_now_add=True, auto_now=False)

    def __str__(self):
        return self.nombre_estudiante