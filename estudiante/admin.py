from django.contrib import admin
from estudiante.models import *

class AdminRegistrarEstudiante(admin.ModelAdmin):
    list_display = ['nombre_estudiante','edad_estudiante','rut_estudiante','carrera','fecha_ingreso']
    search_fields = ['nombre_estudiante','rut_estudiante','carrera']
    #class Meta:
        #model = RegistrarEstudiante


admin.site.register(RegistrarEstudiante,AdminRegistrarEstudiante)
