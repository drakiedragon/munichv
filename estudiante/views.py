from django.shortcuts import render
from .forms import FormRegistroEstudiante
from .models import RegistrarEstudiante

def inicio(request):
    form = FormRegistroEstudiante(request.POST or None)

    if form.is_valid():
        form_data= form.cleaned_data
        nombre_estudiante2 = form_data.get("nombre_estudiante")
        rut_estudiante2 = form_data.get("rut_estudiante")
        edad_estudiante2 = form_data.get("edad_estudiante")
        carrera2 = form.data.get("carrera")
        fecha_ingreso2 = form.data.get("fecha_ingreso")

        objeto = RegistrarEstudiante.objects.create(nombre_estudiante=nombre_estudiante2,rut_estudiante=rut_estudiante2,edad_estudiante=edad_estudiante2,carrera=carrera2,fecha_ingreso=fecha_ingreso2)

    contexto = {
        "el_formulario": form,
    }
    return render(request, "inicio.html", contexto)
