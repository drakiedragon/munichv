from django import forms
from .models import RegistrarEstudiante

class FormRegistroEstudiante(forms.Form):
    nombre_estudiante = forms.CharField(max_length=100)
    rut_estudiante = forms.IntegerField()
    edad_estudiante = forms.IntegerField()
    carrera = forms.CharField(max_length=200)
    fecha_ingreso = forms.DateTimeField()

    class Meta:
        model = RegistrarEstudiante
        fields=["nombre_estudiante","rut_estudiante","edad_estudiante","carrera","fecha_ingreso"]