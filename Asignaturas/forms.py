from django import forms
from .models import RegistrarAsignaturas
class FormRegistroAsignaturas(forms.Form):
    nombre_asignatura = forms.CharField(max_length=100)
    nombre_carrera = forms.CharField()
    semestre = forms.IntegerField()
    fecha_incorporacion = forms.DateTimeField()

    class Meta:
        model = RegistrarAsignaturas
        fields=["nombre_asignatura","nombre_carrera","semestre","fecha_incorporacion"]