from django.contrib import admin
from Asignaturas.models import *

class AdminRegistrarAsignaturas(admin.ModelAdmin):
    list_display = ['nombre_asignatura','nombre_carrera','semestre','fecha_incorporacion']
    search_fields = ['nombre_asignatura','nombre_carrera']
    list_filter = ['nombre_asignatura','nombre_carrera']
admin.site.register(RegistrarAsignaturas,AdminRegistrarAsignaturas)
# Register your models here.
