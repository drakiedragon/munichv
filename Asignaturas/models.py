from django.db import models
class RegistrarAsignaturas(models.Model):
# Create your models here.
    id_asignatura = models.AutoField(primary_key=True,unique=True)
    nombre_asignatura = models.CharField(max_length=100,blank=False)
    nombre_carrera = models.CharField(max_length=100,blank=False)
    semestre = models.IntegerField(blank=False)
    fecha_incorporacion = models.DateTimeField(auto_now_add=True, auto_now=False)

    def __str__(self):
        return self.nombre_asignatura
