# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-04-17 19:07
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='RegistrarAsignaturas',
            fields=[
                ('id_asignatura', models.AutoField(primary_key=True, serialize=False, unique=True)),
                ('nombre_asignatura', models.CharField(max_length=100)),
                ('nombre_carrera', models.CharField(max_length=100)),
                ('fecha_incorporacion', models.DateTimeField(auto_now_add=True)),
            ],
        ),
    ]
