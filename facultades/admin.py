from django.contrib import admin
from facultades.models import *

class AdminRegistrarFacultades(admin.ModelAdmin):
    list_display = ['nombre_facultad','ubicacion_facultad','min_dep_facultad','descripcion_facultad']
    search_fields = ['nombre_facultad','ubicacion_facultad']
    list_filter = ['nombre_facultad','ubicacion_facultad']
    class Meta:
        model = RegistrarFacultades

admin.site.register(RegistrarFacultades,AdminRegistrarFacultades)
