from django import forms
from .models import RegistrarFacultades

class FormRegistroFacultades(forms.Form):
    nombre_facultad = forms.CharField(max_length=100)
    ubicacion_facultad = forms.CharField()
    descripcion_facultad = forms.CharField()
    min_dep_facultad = forms.IntegerField()

    class Meta:
        model = RegistrarFacultades
        fields=["nombre_facultad","ubicacion_facultad","descripcion_facultad","min_dep_facultad"]