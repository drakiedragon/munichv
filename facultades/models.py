from django.db import models

class RegistrarFacultades(models.Model):
    id_facultad = models.AutoField(primary_key=True,unique=True)
    nombre_facultad = models.CharField(max_length=100,blank=False)
    descripcion_facultad = models.CharField(max_length=200,blank=False)
    ubicacion_facultad = models.CharField(max_length=100,blank=False)
    min_dep_facultad = models.IntegerField(blank=False)

    def __str__(self):
        return self.nombre_facultad